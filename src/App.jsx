import React, { useEffect, useState } from "react";
import ReactPaginate from 'react-paginate';
import styled from 'styled-components';

const App = () => {
  const [chat, setChat] = useState([]);

  let limit = 8;
  
  useEffect(() => {
    const url = `https://api.thecatapi.com/v1/images/search?limit=${limit}&page=1&order=DESC`;

    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const json = await response.json();
        setChat(json);
      } catch (error) {
        console.log("error", error);
      }
    };
    fetchData();
  }, [limit]);

  const getChat = async (currentPage) => {
    const response = await fetch(`https://api.thecatapi.com/v1/images/search?limit=${limit}&page=${currentPage}&order=DESC`);
    const json = await response.json();
    return json;
  };

  const clickPage = async (json)=> {
  let currentPage = json.selected + 1;
  const page = await getChat(currentPage);
  setChat(page);
  };


  return (
    <Wrapper>
      <Title>La revanche des chats</Title>
      <div>{chat.map((chat) => (<img src={chat.url} className="card-img-top" key={chat.id} alt="chats en pagaille" />))}</div>
      <ReactPaginate
      previousLabel={'Précédent'}
      nextLabel={'Suivant'}
      breakLabel={'...'}
      pageCount={10}
      onPageChange={clickPage}
      containerClassName={'pagination justify-content-center'}
      pageClassName={'page-item'}
      pageLinkClassName={'page-link'}
      previousClassName={'page-item'}
      previousLinkClassName={'page-link'}
      nextClassName={'page-item'}
      nextLinkClassName={'page-link'}
      breakClassName={'page-item'}
      breakLinkClassName={'page-link'}
      activeClassName={'active'}

      />
    </Wrapper>
  );
};

export default App;

const Wrapper = styled.div`
  padding-top: 50px;
  margin: 0 auto;
`;

const Title = styled.h1`
  font-family: 'Roboto', sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 40px;
  color: #2F131E!important;
  text-align: center;
  margin-bottom: 50px;
`;

